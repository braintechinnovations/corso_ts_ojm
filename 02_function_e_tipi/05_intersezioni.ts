// let studente: {nome: string, cognome: string, matricola: number} = {
//     nome: "Giovanni",
//     cognome: "Pace",
//     matricola: 12345
// }

// let studente: {nome: string} & {cognome: string} & {matricola: number} = {
//     nome: "Giovanni",
//     cognome: "Pace",
//     matricola: 12345
// }


// console.log(studente)

//-----------------------------

let studente: null | {nome: string} = null;
studente = {nome: "Giovanni"}
// studente = {nome: "Giovanni", cognome: "Pace"}