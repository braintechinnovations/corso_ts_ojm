// let professione: "Operaio" | "Professionista" | "Impiegato";
// !!! Durante il transpiling perdo il controllo sui tipi di dato permessi !!!

// professione = "Professionista";
// console.log(professione)

// -------------- ALIAS ----------------
type Punti = 5 | 10 | 20 | 50 | 100;

let score_uno: Punti = 10;
let score_due: Punti = 50;
let score_tre: Punti = 5;

console.log(score_uno + score_due + score_tre)