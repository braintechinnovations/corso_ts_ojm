type Maratona = (kilometri: number) => boolean;

let Corsa: Maratona = function (kilometri: number) : boolean{
    if(kilometri >= 42){
        return true;
    }

    return false;
}

// function Maratona(kilometri: number) : boolean           //Equivalente a scrivere questo

console.log(Corsa(43));

