function dimmiSeSeiMaggiorenne(objPersona) {
    if (objPersona.eta >= 18) {
        console.log("".concat(objPersona.nome, " ").concat(objPersona.cognome, " sei maggiorenne"));
    }
    else {
        console.log("".concat(objPersona.nome, " ").concat(objPersona.cognome, " sei minorenne"));
    }
}
var persona = {
    nome: "GIovanni",
    cognome: "Pace",
    eta: 35
};
dimmiSeSeiMaggiorenne(persona);
