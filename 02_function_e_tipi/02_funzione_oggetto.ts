function dimmiSeSeiMaggiorenne(objPersona: Utente) : void{
    if(objPersona.eta >= 18){
        console.log(`${objPersona.nome} ${objPersona.cognome} sei maggiorenne`)
    }
    else{
        console.log(`${objPersona.nome} ${objPersona.cognome} sei minorenne`)
    }
}

interface Utente{
    nome: string,
    cognome: string,
    eta: number
}

let persona = {
    nome: "GIovanni",
    cognome: "Pace",
    eta: 35
}

dimmiSeSeiMaggiorenne(persona);


