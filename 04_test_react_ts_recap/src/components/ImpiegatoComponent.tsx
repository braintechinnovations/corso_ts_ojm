import React, { useState } from 'react'
import { Col, Form, ListGroup, Row } from 'react-bootstrap'
import { Impiegato } from '../interfacce/Impiegato'

interface Props {
    
}

export const ImpiegatoComponent = (props: Props) => {
    // const [impiegato, setImpiegato] = useState<{nome: string, cognome: string, mansione: string}>();
    const [impiegato, setImpiegato] = useState<Impiegato>()

    const formSubmission = (evt: any) => {
        evt.preventDefault()
    
        let tempImpiegato: Impiegato = {
            nome: evt.target.campoNome.value,
            cognome: evt.target.campoCognome.value,
            mansione: evt.target.campoMansione.value,
        }
    
        setImpiegato(tempImpiegato)
    }

    return (
        <>
        <Row >
            <Col>
                <form onSubmit={formSubmission}>
                    <Form.Group className="mb-3">
                        <Form.Label>Nome</Form.Label>
                        <Form.Control name="inputNome" id="campoNome" type="text" placeholder="Inserisci il valore" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Cognome</Form.Label>
                        <Form.Control name="inputCognome" id="campoCognome" type="text" placeholder="Inserisci il valore" />
                    </Form.Group>
                    <Form.Group className="mb-3">
                        <Form.Label>Mansione</Form.Label>
                        <Form.Control name="inputMansione" id="campoMansione" type="text" placeholder="Inserisci il valore" />
                    </Form.Group>

                    <button type="submit" className="btn btn-success">Inserisci</button>
                </form> 
            </Col>
        </Row>

        <hr />

        {impiegato && (
            <Row>
                <Col>
                <ListGroup as="ul">
                    <ListGroup.Item as="li" active>
                        {impiegato?.nome} {impiegato?.cognome} 
                    </ListGroup.Item>
                    <ListGroup.Item as="li">{impiegato?.mansione}</ListGroup.Item>
                    </ListGroup>
                </Col>
            </Row>
        )}
        
        </>
    )
}
