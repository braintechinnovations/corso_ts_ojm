import React from 'react';
import logo from './logo.svg';
import './App.css';
import { PersonaComponent } from './components/PersonaComponent';
import { StudenteComponent } from './components/StudenteComponent';
import { Anagrafica } from './interfacce/Anagrafica';

import 'bootstrap/dist/css/bootstrap.css'
import { Container } from 'react-bootstrap';
import { ImpiegatoComponent } from './components/ImpiegatoComponent';

function App() {

  let studente: Anagrafica = {
    nome: "Giovanni",
    cognome: "Pace",
    indirizzo: {
      via: "Le mani dal naso",
      cap: 123456,
      citta: "Milano"
    }
  }

  return (
    <Container>
      {/* <PersonaComponent nome="Giovanni" cognome="Pace" />
      <PersonaComponent nome="Mario" cognome={null}/>

      <StudenteComponent anagrafica={studente} /> */}

      <ImpiegatoComponent />
    </Container>
  );
}

export default App;
