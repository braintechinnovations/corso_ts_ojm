export interface Indirizzo{
    via: string
    cap: number
    citta: string
}