import React from 'react'
import { FiglioComponent } from './FiglioComponent'

interface Props {
    
}

export const PadreComponent = (props: Props) => {
    return (
        <div>
            <h1>Sono il padre</h1>

            <FiglioComponent effettuaCambiamento={
                (evento) => {
                    console.log(evento.target.value)
                }
            }/>
        </div>
    )
}
