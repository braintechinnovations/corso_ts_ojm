import React, { useRef } from 'react'

interface Props {
    effettuaCambiamento: (evt: React.ChangeEvent<HTMLInputElement>) => void
}

export const FiglioComponent = (props: Props) => {
    const refNome = useRef<HTMLInputElement>(null)

    return (
        <div>
            <h3>Io sono il figlio</h3>

            <input ref={refNome} onChange={props.effettuaCambiamento}/>
        </div>
    )
}
