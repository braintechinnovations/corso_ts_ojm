import React from 'react';
import logo from './logo.svg';
import './App.css';
import { PadreComponent } from './components/PadreComponent';

function App() {
  return (
    <>
      <PadreComponent />
    </>
  );
}

export default App;
