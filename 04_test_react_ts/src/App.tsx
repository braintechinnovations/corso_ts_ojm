import React from 'react';
import logo from './logo.svg';
import './App.css';
import { PrimoComponent } from './components/PrimoComponent';
import { InoltroProprietaComponent } from './components/InoltroProprietaComponent';
import { ModificheComponent } from './components/ModificheComponent';

function App() {
  let personaTemp = {
    nome: "Mario",
    cognome: "Rossi"
  }

  return (
    <React.Fragment>
      {/* <PrimoComponent /> */}

      {/* <InoltroProprietaComponent testo="Prova Testo" persona={ {nome: "Giovanni", cognome: "Pace"} } />
      <InoltroProprietaComponent persona={personaTemp} />
      <InoltroProprietaComponent /> */}

      <ModificheComponent nominativo="Giovanni Pace"/>
    </React.Fragment>
  );
}

export default App;
