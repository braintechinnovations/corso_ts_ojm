import React from "react";

interface Persona{
    nome: string
    cognome: string
}

interface Props{
    testo?: string
    persona?: Persona
}

export const InoltroProprietaComponent = (props: Props) => {

    console.log(props)

    return (
        <div>
            <h1>Testo:</h1>
            <p>{props.testo ? props.testo : "Non definito"}</p>

            <p>
                Nome: <strong>{props.persona ? props.persona.nome : "Non definito"}</strong>
                <br />
                Cognome: <strong>{props.persona ? props.persona.cognome : "Non definito"}</strong>
            </p>
        </div>
    )

}

