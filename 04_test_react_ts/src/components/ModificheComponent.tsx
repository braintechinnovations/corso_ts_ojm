import React, { useState } from 'react'

interface Props {
    nominativo?: string
}

export const ModificheComponent = (props: Props) => {

    const [nome, setNome] = useState<string | null | undefined>(props.nominativo)
    const [studente, setStudente] = useState<{nome: string, cognome: string}>({
        nome: "Giovanni",
        cognome: "Pace"
    })

    return (
        <div>
            {studente.nome} {studente.cognome}
        </div>
    )
}
