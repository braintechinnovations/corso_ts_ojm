import React from 'react';
import logo from './logo.svg';
import './App.css';
import { DettaglioPersonaComponent } from './components/DettaglioPersonaComponent';
import { ListaPersoneComponent } from './components/ListaPersoneComponent';
import { EliminazioneComponent } from './components/EliminazioneComponent';

function App() {
  return (
    <>
      {/* <DettaglioPersonaComponent idPersona={11} /> */}

      {/* <ListaPersoneComponent /> */}

      <EliminazioneComponent idPersona={18} />
    </>
  );
}

export default App;
