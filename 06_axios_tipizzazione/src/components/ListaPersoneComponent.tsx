import axios from 'axios'
import React, { useState } from 'react'
import { Persona } from '../interfaces/Persona'
import { Config } from './ClasseConfigurazione'
import { PersonaRigaComponent } from './PersonaRigaComponent'

interface Props {
    
}

export const ListaPersoneComponent = (props: Props) => {
    const [elenco, setElenco] = useState<Persona[]>()

    axios.get<Persona[]>(`${Config.url}/list`).then((risultato) => {
        setElenco(risultato.data)
    })
    .catch(errore => console.log(errore))

    return (
        <div>
            <ul>
                {elenco?.map((oggetto, indice) => <PersonaRigaComponent key={indice} persona={oggetto} />)}
            </ul>
        </div>
    )
}
