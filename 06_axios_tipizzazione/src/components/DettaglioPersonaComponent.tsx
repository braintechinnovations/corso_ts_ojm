import axios from 'axios'
import React, { useState } from 'react'
import { Persona } from '../interfaces/Persona';
import { Config } from './ClasseConfigurazione';

interface Props {
    idPersona: number
}

// const url = "http://127.0.0.1:4000/persone";

export const DettaglioPersonaComponent = (props: Props) => {

    const [persona, setPersona] = useState<Persona>();

    axios.get<Persona>(`${Config.url}/${props.idPersona}`).then(
        (risultato) => {
            setPersona(risultato.data)
        }
    ).catch(
        (errore) => {
            console.log(errore)
        }
    )

    //NOT POSSIBLE :(
    // const persona = await axios.get<Persona>(`${url}/${props.idPersona}`);
    // console.log(persona)

    return (
        <div>
            {/* <h1>{persona?.nome} {persona?.cognome} {persona?.telefono}</h1> */}
        </div>
    )
}
