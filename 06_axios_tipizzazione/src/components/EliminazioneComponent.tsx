import axios from 'axios'
import React, { useState } from 'react'
import { Config } from './ClasseConfigurazione'

interface Props {
    idPersona: number
}

interface Risposta{
    status: string
}

export const EliminazioneComponent = (props: Props) => {

    const [risultato, setRisultato] = useState<Risposta>()

//SPOSTARE FUORI DELETE
    axios.delete<Risposta>(`${Config.url}/${props.idPersona}`).then((responso) => {
        setRisultato(responso.data)
        console.log(responso)
    }).catch(errore => console.log(errore))

    return (
        <div>
            {risultato?.status}
        </div>
    )
}
