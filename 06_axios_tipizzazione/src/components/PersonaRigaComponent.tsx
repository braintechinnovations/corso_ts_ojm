import React from 'react'
import { Persona } from '../interfaces/Persona'

interface Props {
    persona: Persona
}

export const PersonaRigaComponent = (props: Props) => {
    return (
        <li>
            {props.persona.id} - {props.persona.nome}  {props.persona.cognome}, {props.persona.telefono} 
        </li>
    )
}
