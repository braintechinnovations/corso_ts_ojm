class Veicolo {
    protected num_ruote: number
    protected targa: string
    protected modello: string
    
    constructor(varRuote: number, varTarga: string, varMode: string){
        this.num_ruote = varRuote
        this.targa = varTarga
        this.modello = varMode
    }

    stampaDettagli(): string{
        return `Numero ruote: ${this.num_ruote}\nTarga: ${this.targa}\nModello: ${this.modello}`
    }
}

class Auto extends Veicolo{

    constructor(vTarga: string, vModel: string){
        super(4, vTarga, vModel)
    }

    get NumRuote(): number{
        return this.num_ruote
    }

}

var lambo = new Auto("LAMBOR", "Aventador")
// lambo.num_ruote = 8
console.log(lambo.stampaDettagli())

var aliscafo = new Veicolo(0, "ABCDE", "Nave");
console.log(aliscafo.stampaDettagli())