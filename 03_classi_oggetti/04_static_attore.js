var Actor = /** @class */ (function () {
    function Actor(varNome, varCognome) {
        this.nome = "N.D.";
        this.cognome = "N.D.";
        this.nome = varNome;
        this.cognome = varCognome;
        Actor.contatore++;
    }
    Actor.contatore = 0;
    return Actor;
}());
var johnny = new Actor("Giovanni", "Pace");
var maria = new Actor("Maria", "Test");
var marco = new Actor("Marco", "Prova");
Actor.contatore = 52;
console.log(Actor.contatore);
