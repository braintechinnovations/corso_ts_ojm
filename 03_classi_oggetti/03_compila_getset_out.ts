class Docente{
    private _nome: string;

    public get nome(): string {
        return this._nome;
    }
    public set nome(value: string) {
        this._nome = value;
    }
}

let martina = new Docente()
martina.nome = "Martina"        //SET
console.log(martina.nome)       //GET