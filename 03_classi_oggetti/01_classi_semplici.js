var Persona = /** @class */ (function () {
    function Persona(varNome, varCognome) {
        this.nome = varNome;
        this.cognome = varCognome;
    }
    Persona.prototype.saluta = function () {
        console.log("Ciao ".concat(this.nome, " ").concat(this.cognome));
    };
    return Persona;
}());
var mar = new Persona("Mario", "Rossi");
mar.saluta();
// let gio = new Persona();
var gio = new Persona(null, null);
gio.nome = "Giovanni";
gio.cognome = "Pace";
gio.saluta();
var val = new Persona("Valeria", null);
val.cognome = "Verdi";
val.saluta();
var mak = new Persona(null, "Giorgi");
mak.nome = "Marika";
mak.saluta();
