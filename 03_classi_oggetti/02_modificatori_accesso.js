//Utilizzate per la compilazione:
//tsc 02_modificatori_accesso.ts --target "ES6" 
class Studente {
    constructor(varNome, varCognome) {
        this.nome = varNome,
            this.cognome = varCognome;
    }
    get Nome() {
        return this.nome ? this.nome.toUpperCase() : "Non definito";
    }
    get Cognome() {
        return this.cognome ? this.cognome.toUpperCase() : "Non definito";
    }
    set Nome(varNome) {
        this.nome = varNome;
    }
    set Cognome(varCognome) {
        this.cognome = varCognome;
    }
    saluta() {
        console.log(`Ciao ${this.nome} ${this.cognome}`);
    }
}
// let giorgio = new Studente("Giorgio", "Pace");
// console.log(giorgio.Nome) 
// // giorgio.saluta();
let maria = new Studente("Maria");
console.log(maria.Nome);
maria.Cognome = "Perinetti";
console.log(maria.Cognome);
