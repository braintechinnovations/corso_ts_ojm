abstract class Animale{
    num_zampe: number
    cammina: boolean
    nuota: boolean
    vola: boolean

    abstract verso(): string
}

class Gatto extends Animale{
    
    verso(){
        return "Meow meow"
    }

}

class Cane extends Animale{

    verso(): string {
        return "Bau Bau"
    }

    

}

let bu = new Gatto();
console.log(bu.verso());

let ares = new Cane();
console.log(ares.verso());
