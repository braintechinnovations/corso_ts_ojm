class Persona{
    nome: string
    cognome: string

    constructor(varNome?: string, varCognome?: string){
        this.nome = varNome;
        this.cognome = varCognome
    }

    saluta() : void{
        console.log(`Ciao ${this.nome} ${this.cognome}`)
    }
}

let mar = new Persona("Mario", "Rossi")
mar.saluta();

// let gio = new Persona();
let gio = new Persona(null, null);
gio.nome = "Giovanni"
gio.cognome = "Pace"
gio.saluta();

let val = new Persona("Valeria", null);
val.cognome = "Verdi"
val.saluta();

let mak = new Persona(null, "Giorgi");
mak.nome = "Marika"
mak.saluta();