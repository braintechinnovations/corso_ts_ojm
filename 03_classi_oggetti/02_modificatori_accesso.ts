//Utilizzate per la compilazione:
//tsc 02_modificatori_accesso.ts --target "ES6" 

class Studente{

    private nome: string
    private cognome: string

    constructor(varNome?: string, varCognome?: string){
        this.nome = varNome,
        this.cognome = varCognome
    }

    get Nome(): string{
        return this.nome ? this.nome.toUpperCase() : "Non definito"
    }
    get Cognome(): string{
        return this.cognome ? this.cognome.toUpperCase() : "Non definito"
    }

    set Nome(varNome: string) {
        this.nome = varNome
    }

    set Cognome(varCognome: string) {
        this.cognome = varCognome
    }

    saluta(): void{
        console.log(`Ciao ${this.nome} ${this.cognome}`)
    }

}

// let giorgio = new Studente("Giorgio", "Pace");
// console.log(giorgio.Nome) 
// // giorgio.saluta();

let maria = new Studente("Maria")
console.log(maria.Nome)

maria.Cognome = "Perinetti";
console.log(maria.Cognome)