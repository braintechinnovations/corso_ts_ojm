class Veicolo {
    constructor(varRuote, varTarga, varMode) {
        this.num_ruote = varRuote;
        this.targa = varTarga;
        this.modello = varMode;
    }
    stampaDettagli() {
        return `Numero ruote: ${this.num_ruote}\nTarga: ${this.targa}\nModello: ${this.modello}`;
    }
}
class Auto extends Veicolo {
    constructor(vTarga, vModel) {
        super(4, vTarga, vModel);
    }
    get NumRuote() {
        return this.num_ruote;
    }
}
var lambo = new Auto("LAMBOR", "Aventador");
// lambo.num_ruote = 8
console.log(lambo.stampaDettagli());
var aliscafo = new Veicolo(0, "ABCDE", "Nave");
console.log(aliscafo.stampaDettagli());
