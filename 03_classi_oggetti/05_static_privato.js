class Attore {
    constructor(varNome, varCognome) {
        this.nome = "N.D.";
        this.cognome = "N.D.";
        this.nome = varNome;
        this.cognome = varCognome;
        Attore.contatore++;
    }
    // public static restituisciContatore(): number{
    //     return Attore.contatore
    // }
    static get Contatore() {
        return Attore.contatore;
    }
}
Attore.contatore = 0;
console.log(Attore.Contatore);
let johnny = new Attore("Giovanni", "Pace");
let maria = new Attore("Maria", "Test");
let marco = new Attore("Marco", "Prova");
console.log(Attore.Contatore);
