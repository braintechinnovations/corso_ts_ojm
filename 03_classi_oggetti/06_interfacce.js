var giov = {
    nome: "Giovanni",
    cognome: "Pace",
    generaIdentificativo: function () {
        var id = "dir-".concat(giov.nome.toLowerCase()).concat(giov.cognome.toLowerCase());
        return id;
    }
};
console.log(giov.generaIdentificativo());
var mari = {
    nome: "Mario",
    cognome: "Rossi",
    generaIdentificativo: function () {
        var id = "ope-".concat(this.nome.toLowerCase()).concat(this.cognome.toLowerCase());
        return id;
    }
};
console.log(mari.generaIdentificativo());
