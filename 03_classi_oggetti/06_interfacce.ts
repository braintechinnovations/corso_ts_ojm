interface Impiegato{
    nome: string
    cognome: string
    generaIdentificativo: () => string 
}

let giov: Impiegato = {
    nome: "Giovanni",
    cognome: "Pace",
    generaIdentificativo: () => {
        let id = `dir-${giov.nome.toLowerCase()}${giov.cognome.toLowerCase()}`;
        return id;
    }
}

console.log(giov.generaIdentificativo())

let mari: Impiegato = {
    nome: "Mario",
    cognome: "Rossi",
    generaIdentificativo: function (): string {
        let id = `ope-${this.nome.toLowerCase()}${this.cognome.toLowerCase()}`;
        return id;
    }
}

console.log(mari.generaIdentificativo())