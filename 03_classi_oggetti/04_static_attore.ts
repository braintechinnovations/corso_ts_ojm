class Actor{
    private nome: string = "N.D."
    private cognome: string = "N.D."

    static contatore: number = 0

    constructor(varNome?: string, varCognome?: string){
        this.nome = varNome;
        this.cognome = varCognome;
        Actor.contatore++;
    }
}

let johnny = new Actor("Giovanni", "Pace")
let maria = new Actor("Maria", "Test")
let marco = new Actor("Marco", "Prova")

// Actor.contatore = 52;               //NON DEVE ESSERE MAI PERMESSO

console.log(Actor.contatore)