interface Player{
    play(): void
    pausa(): void
    stop(): void
}

class Spotify implements Player{

    play(): void {
        console.log("Ho cliccato Play")
    }
    pausa(): void {
        console.log("Ho cliccato Pausa")
    }
    stop(): void {
        console.log("Ho cliccato Stop")
    }
    
}

class GooglePlayMusic implements Player{
    
    play(): void {
        throw new Error("Method not implemented.")
    }
    pausa(): void {
        throw new Error("Method not implemented.")
    }
    stop(): void {
        throw new Error("Method not implemented.")
    }
    
}