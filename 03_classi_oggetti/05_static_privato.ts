class Attore{
    private nome: string = "N.D."
    private cognome: string = "N.D."

    private static contatore: number = 0

    constructor(varNome?: string, varCognome?: string){
        this.nome = varNome;
        this.cognome = varCognome;
        Attore.contatore++;
    }

    // public static restituisciContatore(): number{
    //     return Attore.contatore
    // }

    public static get Contatore(): number{
        return Attore.contatore
    }
}

console.log(Attore.Contatore)

let johnny = new Attore("Giovanni", "Pace")
let maria = new Attore("Maria", "Test")
let marco = new Attore("Marco", "Prova")

console.log(Attore.Contatore)


// class DBString {
//     private static host: string = "127.0.0.1"
//     private static username: string = "Ciccio"
//     private static password: string = "Pasticcio"

//     get Host(): string{
//         return DBString.host
//     }
// }

// console.log(DBString.Host)
